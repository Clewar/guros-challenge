const hasMutation = (dna) => {
  let matches = 0
  const length = dna.length

  for (let i = 0; i < length; i++) {
    if (dna[i].length === length) {
      for (let j = 0; j < length; j++) {
        if ('ATCG'.includes(dna[i].charAt(j))) {
          if (checkIfMatch(j, i, 'right', 1, dna, length)) {
            matches++
          }
          if (checkIfMatch(j, i, 'down', 1, dna, length)) {
            matches++
          }
          if (checkIfMatch(j, i, 'diagRight', 1, dna, length)) {
            matches++
          }
          if (checkIfMatch(j, i, 'diagLeft', 1, dna, length)) {
            matches++
          }
        } else {
          throw 'Matrix must contain only A,T,C or G'
        }
      }
    } else {
      throw 'Matrix must be squared'
    }
  }
  return matches > 1
}

const checkIfMatch = (x, y, direction, count, dna, length) => {
  if (count === 4) {
    return true
  }

  switch (direction) {
    case 'right':
      if (dna[y]?.charAt(x + 1) === dna[y].charAt(x)) {
        return checkIfMatch(x + 1, y, direction, count + 1, dna, length)
      }
      break
    case 'down':
      if (dna[y + 1]?.charAt(x) === dna[y].charAt(x)) {
        return checkIfMatch(x, y + 1, direction, count + 1, dna, length)
      }
      break
    case 'diagRight':
      if (dna[y + 1]?.charAt(x + 1) === dna[y].charAt(x)) {
        return checkIfMatch(x + 1, y + 1, direction, count + 1, dna, length)
      }
      break
    case 'diagLeft':
      if (dna[y + 1]?.charAt(x - 1) === dna[y].charAt(x)) {
        return checkIfMatch(x - 1, y + 1, direction, count + 1, dna, length)
      }
      break
  }
  return false
}

module.exports = { hasMutation }
