const DNA = require('../../src/models/dna')

const setupDatabase = async () => {
  await DNA.deleteMany()
}

module.exports = setupDatabase
