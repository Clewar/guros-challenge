const request = require('supertest')
const app = require('../src/app')
const mutation = require('../utils/hasMutation')
const DNA = require('../src/models/dna')
const setupDatabase = require('./fixtures/db')
const mongoose = require('mongoose')

beforeAll(setupDatabase)

afterAll(async () => await mongoose.connection.close())

test('Should find mutations', () => {
  const diagonalDiagonal = mutation.hasMutation([
    'ATGCGA',
    'CAGTAC',
    'TTAAGT',
    'AGAAGG',
    'CGCCTA',
    'TCACTG',
  ])
  expect(diagonalDiagonal).toBe(true)

  const diagonalHorizontal = mutation.hasMutation([
    'ATGCGA',
    'CAGTGC',
    'TTATGT',
    'AGAAGG',
    'CCCCTA',
    'TCACTG',
  ])
  expect(diagonalHorizontal).toBe(true)

  const horizontalVertical = mutation.hasMutation([
    'ATGCGA',
    'CAGTGC',
    'TTATGT',
    'AGACGG',
    'CCCCTA',
    'TCACTG',
  ])
  expect(horizontalVertical).toBe(true)
})

test('Should not find mutations', () => {
  const noMatch = mutation.hasMutation([
    'ATGCGA',
    'CAGTGC',
    'TTATTT',
    'AGACGG',
    'GCGTCA',
    'TCACTG',
  ])
  expect(noMatch).toBe(false)

  const oneMatch = mutation.hasMutation([
    'ATGCGA',
    'CAGTGC',
    'TTATTT',
    'AGAAGG',
    'GCGTCA',
    'TCACTG',
  ])
  expect(oneMatch).toBe(false)
})

test('Should return 200 on mutated dna and save it to database', async () => {
  const response = await request(app)
    .post('/mutation')
    .send({
      dna: ['ATGCGA', 'CAGTAC', 'TTAAGT', 'AGAAGG', 'CGCCTA', 'TCACTG'],
    })
    .expect(200)

  totalMutatedDNAs = await DNA.countMutatedDNAs()
  expect(totalMutatedDNAs).toBe(1)
})

test('Should return 403 on mutated dna and save it to database', async () => {
  const response = await request(app)
    .post('/mutation')
    .send({
      dna: ['ATGCGA', 'CAGTGC', 'TTATTT', 'AGACGG', 'GCGTCA', 'TCACTG'],
    })
    .expect(403)

  const response2 = await request(app)
    .post('/mutation')
    .send({
      dna: ['CTGCGA', 'CAGTGC', 'TTATTT', 'AGAAGG', 'GCGTCA', 'TCACTG'],
    })
    .expect(403)

  totalNotMutatedDNAs = await DNA.countNotMutatedDNAs()
  expect(totalNotMutatedDNAs).toBe(2)
})

test('Return ratio with 200 status', async () => {
  const response = await request(app).get('/stats').expect(200)

  expect(response.body.count_mutations).toBe(1)
  expect(response.body.count_no_mutation).toBe(2)
  expect(response.body.ratio).toBe(1 / 3)
})
