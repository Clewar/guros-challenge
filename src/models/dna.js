const mongoose = require('mongoose')

const dnaSchema = new mongoose.Schema(
  {
    sequence: {
      type: Array,
      required: true,
      trim: true,
    },
    mutated: {
      type: Boolean,
      required: true,
    },
  },
  {
    timestamps: true,
  }
)

dnaSchema.statics.countMutatedDNAs = async function () {
  return await DNA.countDocuments({ mutated: true })
}

dnaSchema.statics.countNotMutatedDNAs = async function () {
  return await DNA.countDocuments({ mutated: false })
}

const DNA = mongoose.model('DNA', dnaSchema)

module.exports = DNA
