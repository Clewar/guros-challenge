const express = require('express')
const router = new express.Router()
const mutation = require('../../utils/hasMutation')
const DNA = require('../models/dna')

router.get('/', (req, res) => {
  res.send('Hello Guros!')
})

router.post('/mutation', async (req, res) => {
  let dna = new DNA({ sequence: req.body.dna })
  try {
    if (mutation.hasMutation(dna.sequence)) {
      dna.mutated = true
      res.status(200).send('Mutation detected')
    } else {
      dna.mutated = false
      res.status(403).send('No mutation detected')
    }
    await dna.save()
  } catch (error) {
    res.status(400).send(error)
  }
})

router.get('/stats', async (req, res) => {
  totalMutatedDNAs = await DNA.countMutatedDNAs()
  totalNotMutatedDNAs = await DNA.countNotMutatedDNAs()
  ratio = totalMutatedDNAs / (totalMutatedDNAs + totalNotMutatedDNAs)
  res.status(200).send({
    count_mutations: totalMutatedDNAs,
    count_no_mutation: totalNotMutatedDNAs,
    ratio: ratio,
  })
})

module.exports = router
