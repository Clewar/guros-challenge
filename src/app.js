require('./db/mongoose')
const express = require('express')
const bodyParser = require('body-parser')
const mutationRouter = require('./routers/mutation')

const app = express()

app.use(bodyParser.json())
app.use(mutationRouter)

module.exports = app
