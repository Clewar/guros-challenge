# guros-challenge



## Introduction

This is the project made for the guros challenge

## Dependencies

- Node 16.13.0
- MongoDB 5.0.4

## Commands to run

`npm i`

`mongod`

`mkdir config`

`echo 'PORT=8003' > config/test.env`

`echo 'MONGODB_URL=mongodb://127.0.0.1:27017/guros-challenge-api' >> config/test.env`

`npm run dev`

## Annotations

The code on this program prioritized speed and efficiency over readable code.
For example, on the hasMutation() method it is reviewed if the matrix is squared at the same time it is iterating through it when searching for 4 of the same characters in a row, column or diagonal. This way we only iterate through the array once instead of twice if we made the review process into a different function.

Also in the checkIfMatch method we pass through the lenght so we don't have to calculate everytime the function is called.
If we wanted to make the code more readable at the cost of efficiency we could make separate methods for the matrix navigation instead of having dna\[y\]?.charAt(x), not passing the lenght on every call and calculate it each time and having a separate method for validating the matrix being squared or containing only the allowed characters.

- Isaí